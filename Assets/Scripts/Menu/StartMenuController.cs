using FruitNinja.Menu.Controller;
using FruitNinja.UI.Button;
using System;
using UnityEngine;

namespace FruitNinja.Menu.Start
{
    public class StartMenuController : MenuController
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button quitButton;

        public event Action StartPressed;
        public event Action QuitPressed;

        private void Start()
        {
            startButton.ButtonReleased += OnStart;
            quitButton.ButtonReleased += OnQuit;
        }
        private void OnDestroy()
        {
            startButton.ButtonReleased -= OnStart;
            quitButton.ButtonReleased -= OnQuit;
        }
        private void OnStart()
        {
            StartPressed?.Invoke();
        }
        private void OnQuit()
        {
            QuitPressed?.Invoke();
        }
    }
}
