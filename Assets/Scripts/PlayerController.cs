using FruitNinja.Item.Controller;
using System;
using UnityEngine;

namespace FruitNinja.Player.Controller
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;

        private Ray raycast;
        private RaycastHit2D hit;

        private void Update()
        {
            PlayerInput();
        }

        private void PlayerInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                raycast = mainCamera.ScreenPointToRay(Input.mousePosition);
                hit = Physics2D.GetRayIntersection(raycast);

                if (hit.collider != null)
                {
                    hit.collider.GetComponent<ItemController>().Interact();
                }
            }
        }
    }
}