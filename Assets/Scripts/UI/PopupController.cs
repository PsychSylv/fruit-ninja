using UnityEngine;

namespace FruitNinja.UI.Popup.Controller
{
    public abstract class PopupController : MonoBehaviour
    {
        public Canvas canvas;

        public virtual void ShowCanvas()
        {
            gameObject.SetActive(true);
            canvas.enabled = true;
        }
        public virtual void HideCanvas()
        {
            gameObject.SetActive(false);
            canvas.enabled = false;
        }
    }
}
