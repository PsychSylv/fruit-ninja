using FruitNinja.Game.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace FruitNinja.UI.Controller
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text timerText;

        private void Start()
        {
            gameManager.ScoreChanged += OnScoreChanged;
            gameManager.TimerChanged += OnTimerChanged;
        }

        private void OnDestroy()
        {
            gameManager.ScoreChanged -= OnScoreChanged;
            gameManager.TimerChanged -= OnTimerChanged;
        }

        private void OnScoreChanged(int score)
        {
            scoreText.text = "Score: " +  score.ToString();
        }

        private void OnTimerChanged(float time)
        {
            timerText.text = "Time Left: " + ((int)time).ToString();
        }
    }
}
