using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FruitNinja.UI.Button
{
    public class Button : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Image buttonImage;
        [SerializeField] private Sprite normalButtonSprite;
        [SerializeField] private Sprite pressedButtonSprite;

        public event Action ButtonClicked;
        public event Action ButtonReleased;

        public virtual void Start()
        {
            ButtonReleased += OnButtonNormal;
            ButtonClicked += OnButtonPressed;
        }
        public virtual void OnDestroy()
        {
            ButtonReleased -= OnButtonNormal;
            ButtonClicked -= OnButtonPressed;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            ButtonClicked?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ButtonReleased?.Invoke();
        }
        public virtual void OnButtonNormal()
        {
            buttonImage.sprite = normalButtonSprite;
        }
        public virtual void OnButtonPressed()
        {
            buttonImage.sprite = pressedButtonSprite;
        }
    }
}
