using FruitNinja.Game.State;
using FruitNinja.Item.Pool;
using FruitNinja.Menu.Start;
using FruitNinja.Player.Controller;
using FruitNinja.UI.Popup.GameOver;
using FruitNinja.UI.Popup.Pause;
using System;
using System.Collections;
using UnityEngine;


namespace FruitNinja.Game.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private StartMenuController startMenuController;
        [SerializeField] private PausePopupController pausePopupController;
        [SerializeField] private GameOverPopupController gameOverPopupController;
        [SerializeField] private ItemPoolController itemPoolController;
        [SerializeField] private PlayerController playerController;
        [SerializeField] private GameState gameState;

        [SerializeField] private int firstCheckpointTotal;
        [SerializeField] private int secondCheckpointTotal;
        [SerializeField] private int thirdCheckpointTotal;

        private float time = 60f;
        private float spawnTime = 4f;
        private int score = 0;
        private int spawnValue = 3;
        private int coroutineCounter = 0;

        private const int InitialSpawnValue = 3;
        private const float InitialSpawnTime = 4f;
        private const float MaxTimer = 60f;
        private const float MinusTimer = 3f;
        private const int FirstCheckPoint = 3;
        private const int SecondCheckPoint = 8;
        private const int ThirdCheckPoint = 16;

        public event Action<float> TimerChanged;
        public event Action<int> ScoreChanged;
        public event Action<bool> ItemHitPlayed;
        public event Action PausePlayed;
        public event Action GameOverPlayed;

        private void Start()
        {
            startMenuController.StartPressed += StartGame;
            startMenuController.QuitPressed += OnQuit;

            itemPoolController.ItemHit += OnHitItem;

            pausePopupController.ContinuePressed += OnResume;
            pausePopupController.RestartPressed += OnRestart;
            pausePopupController.ExitPressed += OnExit;

            gameOverPopupController.PlayAgainPressed += OnRestart;
            gameOverPopupController.ExitPressed += OnExit;

            gameState = GameState.Home;
        }

        private void OnDestroy()
        {
            startMenuController.StartPressed -= StartGame;
            startMenuController.QuitPressed -= OnQuit;

            pausePopupController.ContinuePressed -= OnResume;
            pausePopupController.RestartPressed -= OnRestart;
            pausePopupController.ExitPressed -= OnExit;

            gameOverPopupController.PlayAgainPressed -= OnRestart;
            gameOverPopupController.ExitPressed -= OnExit;
        }

        private void Update()
        {
            if(gameState == GameState.Playing || gameState == GameState.Paused)
            {
                if(Input.GetKeyDown(KeyCode.Escape))
                {
                    Pause();
                }
            }

            if(gameState == GameState.Playing)
            {
                GameTimer();
            }
        }

        private void OnHitItem(String tag)
        {
            if(tag == "Fruit")
            {
                score++;
                ScoreChanged?.Invoke(score);
                ItemHitPlayed?.Invoke(true);
            }
            else
            {
                time -= MinusTimer;
                TimerChanged?.Invoke(time);
                ItemHitPlayed?.Invoke(false);
            }
        }

        private void StartGame()
        {
            StopAllCoroutines();
            itemPoolController.ResetPool();

            startMenuController.HideCanvas();
            pausePopupController.HideCanvas();
            gameOverPopupController.HideCanvas();

            gameState = GameState.Playing;

            spawnTime = InitialSpawnTime;
            spawnValue = InitialSpawnValue;
            score = 0;
            coroutineCounter = 0;
            time = MaxTimer;

            ScoreChanged?.Invoke(score);
            TimerChanged?.Invoke(time);
            StartCoroutine(ItemSpawn(spawnTime));
        }
        
        private IEnumerator ItemSpawn(float time)
        {
            yield return new WaitForSeconds(time);
            itemPoolController.GetPooledItem(spawnValue);
            coroutineCounter++;

            if(coroutineCounter == FirstCheckPoint || coroutineCounter == SecondCheckPoint || coroutineCounter == ThirdCheckPoint)
            {
                spawnValue++;
                spawnTime--;

                if(coroutineCounter == FirstCheckPoint)
                {
                    itemPoolController.ExpandPool(firstCheckpointTotal);
                }
                else if(coroutineCounter == SecondCheckPoint)
                {
                    itemPoolController.ExpandPool(secondCheckpointTotal);
                }
                else if(coroutineCounter == ThirdCheckPoint)
                {
                    itemPoolController.ExpandPool(thirdCheckpointTotal);
                }
            }

            StartCoroutine(ItemSpawn(spawnTime));
        }

        private void GameTimer()
        {
            time -= Time.deltaTime;
            TimerChanged?.Invoke(time);

            if(time <= 0)
            {
                GameOverCheck();
            }
        }

        private void Pause()
        {
            if(gameState != GameState.Paused)
            {
                Time.timeScale = 0f;
                pausePopupController.ShowCanvas();
                gameState = GameState.Paused;
                PausePlayed?.Invoke();
            }
            else
            {
                OnResume();
            }
        }

        private void GameOverCheck()
        {
            gameOverPopupController.ShowCanvas();
            gameState = GameState.GameOver;
            StopAllCoroutines();

            GameOverPlayed?.Invoke();
        }

        private void OnResume()
        {
            Time.timeScale = 1f;
            pausePopupController.HideCanvas();
            gameState = GameState.Playing;
        }

        private void OnRestart()
        {
            Time.timeScale = 1f;
            StartGame();
        }

        private void OnExit()
        {
            Time.timeScale = 1f;

            startMenuController.ShowCanvas();
            pausePopupController.HideCanvas();
            gameOverPopupController.HideCanvas();

            itemPoolController.ResetPool();
            StopAllCoroutines();

            gameState = GameState.Home;
        }

        private void OnQuit()
        {
            Application.Quit();
        }
    }
}