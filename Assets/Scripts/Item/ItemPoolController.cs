using FruitNinja.Item.Controller;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.Item.Pool
{
    public class ItemPoolController : MonoBehaviour
    {
        [SerializeField] private List<ItemController> itemControllers;
        [SerializeField] private ItemController item;
        [SerializeField] private Transform itemHolder;
        [SerializeField] private int itemInitialPoolValue;

        private int countLimit = 0;

        public event Action<string> ItemHit;

        private void Start()
        {
            itemControllers = new List<ItemController>(new ItemController[itemInitialPoolValue]);

            for (int i = 0; i < itemInitialPoolValue; i++)
            {
                itemControllers[i] = Instantiate(item, transform.position, Quaternion.identity, itemHolder);
                itemControllers[i].gameObject.SetActive(false);
                itemControllers[i].ItemHit += OnHitItem;
            }
        }

        private void OnDestroy()
        {
            for(int i = 0; i < itemControllers.Count; i++)
            {
                itemControllers[i].ItemHit -= OnHitItem;
            }
        }

        public void GetPooledItem(int poolValue)
        {
            countLimit = 0;
            for (int i = 0; i < itemControllers.Count; i++)
            {
                if (!itemControllers[i].gameObject.activeInHierarchy)
                {
                    if (countLimit > poolValue) return;

                    itemControllers[i].transform.position = transform.position;
                    itemControllers[i].Activate();
                    countLimit++;
                }
            }
        }

        public void ExpandPool(int total)
        {
            if (total <= itemControllers.Count) return;
            else
            {
                var totalToSpawn = total - itemControllers.Count;
                
                for (int i = totalToSpawn; i > 0; i--)
                {
                    itemControllers.Add(item);
                    var itemIndex = itemControllers.Count - 1;

                    itemControllers[itemIndex] = Instantiate(item, transform.position, Quaternion.identity, itemHolder);
                    itemControllers[itemIndex].gameObject.SetActive(false);
                    itemControllers[itemIndex].ItemHit += OnHitItem;
                }
            }
        }

        public void ResetPool()
        {
            for (int i = 0; i < itemControllers.Count; i++)
            {
                itemControllers[i].Deactivate();
            }
        }

        private void OnHitItem(String item)
        {
            ItemHit?.Invoke(item);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            collision.gameObject.GetComponent<ItemController>().Deactivate();
        }
    }
}
