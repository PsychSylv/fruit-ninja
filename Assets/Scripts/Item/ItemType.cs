namespace FruitNinja.Item.Type
{
    public enum ItemType
    {
        None,
        Fruit,
        Bomb
    }
}
