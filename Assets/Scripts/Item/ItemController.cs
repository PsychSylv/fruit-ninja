using FruitNinja.Item.Type;
using System;
using UnityEngine;

namespace FruitNinja.Item.Controller
{
    public class ItemController : MonoBehaviour
    {
        [SerializeField] private Sprite bombSprite;
        [SerializeField] private Sprite[] fruitSprite;
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Rigidbody2D rigidBody;

        private bool isActive = true;
        private int random;
        private Color activeColor = new Color(1, 1, 1, 1);
        private Color inactiveColor = new Color(1, 1, 1, 0.3f);

        private const int MinimumRandomValue = 0;
        private const int MaximumRandomValue = 4;
        private const float MinimumXRandomValue = -5f;
        private const float MaximumXRandomValue = 5f;
        private const float MinimumYRandomValue = 15f;
        private const float MaximumYRandomValue = 20f;

        public event Action<string> ItemHit;

        public bool IsActive
        {
            get { return isActive; }
            private set { isActive = value; ChangeColor(value); }
        }

        public void Interact()
        {
            if(IsActive)
            {
                ItemHit?.Invoke(gameObject.tag);
                IsActive = false;
            }
        }

        public void Activate()
        {
            RandomizeItem();
            gameObject.SetActive(true);
            IsActive = true;
            LaunchItem();
        }

        public void Deactivate()
        {
            if(gameObject.activeInHierarchy)
            {
                gameObject.SetActive(false);
            }
        }

        private void RandomizeItem()
        {
            random = UnityEngine.Random.Range(MinimumRandomValue, MaximumRandomValue);

            if(random == 0)
            {
                spriteRenderer.sprite = bombSprite;
                gameObject.tag = "Bomb";
            }
            else
            {
                spriteRenderer.sprite = fruitSprite[UnityEngine.Random.Range(MinimumRandomValue, fruitSprite.Length)];
                gameObject.tag = "Fruit";
            }
        }
        private void LaunchItem()
        {
            rigidBody.AddForce(transform.up * UnityEngine.Random.Range(MinimumYRandomValue, MaximumYRandomValue), ForceMode2D.Impulse);
            rigidBody.AddForce(transform.right * UnityEngine.Random.Range(MinimumXRandomValue, MaximumXRandomValue), ForceMode2D.Impulse);
        }

        private void ChangeColor(bool active)
        {
            if (active) spriteRenderer.color = activeColor;
            else spriteRenderer.color = inactiveColor;
        }
    }
}
