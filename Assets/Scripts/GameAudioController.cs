using FruitNinja.Game.Manager;
using UnityEngine;

namespace FruitNinja.Audio.Controller
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip fruitHitClip;
        [SerializeField] private AudioClip bombHitClip;
        [SerializeField] private AudioClip pauseClip;
        [SerializeField] private AudioClip gameOverClip;

        private void Start()
        {
            gameManager.ItemHitPlayed += OnItemHit;
            gameManager.PausePlayed += OnPaused;
            gameManager.GameOverPlayed += OnGameOver;
        }

        private void OnDestroy()
        {
            gameManager.ItemHitPlayed -= OnItemHit;
            gameManager.PausePlayed -= OnPaused;
            gameManager.GameOverPlayed -= OnGameOver;
        }

        private void OnItemHit(bool fruit)
        {
            if(fruit) audioSource.PlayOneShot(fruitHitClip);
            else audioSource.PlayOneShot(bombHitClip);
        }

        private void OnPaused()
        {
            audioSource.PlayOneShot(pauseClip);
        }

        private void OnGameOver()
        {
            audioSource.PlayOneShot(gameOverClip);
        }
    }
}
